var NAVTREE =
[
  [ "cds", "index.html", [
    [ "CDS: Concurrent Data Structures library", "index.html", null ],
    [ "Synchronization monitor", "cds_sync_monitor.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", "functions_type" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classcds_1_1container_1_1_bronson_a_v_l_tree_map_3_01cds_1_1urcu_1_1gc_3_01_r_c_u_01_4_00_01_key_00_01_t_00_01_traits_01_4.html#a1f5e5bdd93442d44bf7a188f85530856",
"classcds_1_1container_1_1_cuckoo_set.html#a9ba8f1383336b8f15853911a4414cb3f",
"classcds_1_1container_1_1_ellen_bin_tree_set.html#a9db486ca0072c2716384c9532ef78bf0",
"classcds_1_1container_1_1_f_c_stack.html#adfb7f6d966cf6326d8e1402523c4c614",
"classcds_1_1container_1_1_iterable_k_v_list.html#a74ff2d752e5e4b15753945e1aaf97688",
"classcds_1_1container_1_1_lazy_list_3_01cds_1_1gc_1_1nogc_00_01_t_00_01_traits_01_4.html#a4a10124b7f6a0bbeef5d631f721cefee",
"classcds_1_1container_1_1_michael_hash_set.html#a15739d30cb993eb0a62e7d091045f094",
"classcds_1_1container_1_1_michael_list.html#a13095c80b006a7919e66554a01b5a895",
"classcds_1_1container_1_1_skip_list_map.html#ac06c1bca1a241ac2d40190717f203103",
"classcds_1_1container_1_1_split_list_map.html#a8655d4fe74cc3f9aebb5de8e8217a38f",
"classcds_1_1container_1_1_striped_map.html#abd563bbd37f3b08f2763c3cff9abaf49",
"classcds_1_1gc_1_1_d_h_p_1_1_guard_array.html#ac3c4bbf1444e3ae24e6e26a37af2fd3c",
"classcds_1_1intrusive_1_1_ellen_bin_tree.html#a66fef3313f95b887854de03dd7032140",
"classcds_1_1intrusive_1_1_feldman_hash_set.html#a87391ff3aa9632b6ce0bd776063e1f69",
"classcds_1_1intrusive_1_1_lazy_list_3_01gc_1_1nogc_00_01_t_00_01_traits_01_4.html#a9a6e90d9de78c18962fdf9e97f3f11a0",
"classcds_1_1intrusive_1_1_michael_list_3_01cds_1_1urcu_1_1gc_3_01_r_c_u_01_4_00_01_t_00_01_traits_01_4.html#a11969ccc97da2c07c9b734d9f5955406",
"classcds_1_1intrusive_1_1_skip_list_set_3_01cds_1_1urcu_1_1gc_3_01_r_c_u_01_4_00_01_t_00_01_traits_01_4.html#a0ad0e95249f8442e4b28accd24d9479b",
"classcds_1_1intrusive_1_1_treiber_stack.html#a34937a52801400187ecf9f611456047f",
"classcds_1_1opt_1_1v_1_1uninitialized__static__buffer.html#a0690a93bd00eecad00f07f84d71a72a5",
"classcds_1_1urcu_1_1signal__buffered.html#a90c4149d11c8561d7a706f8aae443b98",
"namespacecds_1_1gc.html",
"structcds_1_1container_1_1bronson__avltree_1_1stat.html#a5dca860a8a5c4acb8f93ff1a3115096c",
"structcds_1_1container_1_1treiber__stack_1_1traits.html#ab06c029e3577cae4be13b435289f6cce",
"structcds_1_1intrusive_1_1ellen__bintree_1_1stat.html#ab1f4d5da25caf95ad49c2195f53f8f17",
"structcds_1_1intrusive_1_1michael__list_1_1traits.html#ac2d820b2374e7d3264c824e55883faa0",
"structcds_1_1intrusive_1_1split__list_1_1traits.html#a58411f42a842b775d8969aac252acbd9"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';